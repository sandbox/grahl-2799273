<?php

$query = \Drupal::entityQuery('taxonomy_term');
$query->condition('vid', 'YOUR_VOCABULARY');
$tids = $query->execute();
$terms = \Drupal\taxonomy\Entity\Term::loadMultiple($tids);

$csv = array_map('str_getcsv', file('input.csv'));
array_walk($csv, function(&$a) use ($csv) {
  $a = array_combine($csv[0], $a);
});
array_shift($csv);

$processed_input = [];
foreach ($csv as $line) {
  $processed_input[$line['de']] = $line;
}

$languages = ['en', 'fr', 'it'];

/**
 * @param $term
 * @param $processed_input
 * @return mixed
 */
function saveTermTranslation($term, $data, $language) {
  /* @var $term \Drupal\taxonomy\Entity\Term */
  if (!$term->hasTranslation($language)) {
    $translation = $term->addTranslation($language);
    $translation->setName($data);
    $translation->save();
  }
}

foreach ($terms as $term) {
  /* @var $term \Drupal\taxonomy\Entity\Term */
  if (array_key_exists($term->getName(), $processed_input)) {
    echo 'Processing '. $term->getName() . ' (' . $term->language()->getId() . ')' .  PHP_EOL;
    echo 'Input given:' . implode(';', $processed_input[$term->getName()]) . PHP_EOL;
    foreach ($languages as $language) {
      saveTermTranslation($term, $processed_input[$term->getName()][$language], $language);
    }
  }
}
